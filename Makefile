all:

NODE_VERSIONS="6 8 10 12 14 16"

node:
	@for v in "${NODE_VERSIONS}"; \
	do \
		dir="node$${v}"; \
		mkdir -p "$${dir}"; \
		sed -e "s/_version_/$${v}/g" < Dockerfile.node > "$${dir}"/Dockerfile; \
	done
